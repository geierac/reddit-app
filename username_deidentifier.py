# module for coverting Reddit usernames into aliases

# parameter: username. Should make a dictionary of usernames where username is matched to alias. First time it sees a username, should add it to the dictionary and asign an alias which hasn't been used before and then return the alias. If it's seen a username before, it should fetch the alias associated with it and return that.

# username = "pandapurjo"
#
# usernames = ["shady12", "pandapurjo", "Epona3008", "pandapurjo", "iamanundertaker", "iridiumhatandcoat", "Raexis", "JamesNinelives", "pandapurjo"]

username_to_alias = {}
count = 1

def deidentify(username):
    count = len(username_to_alias.keys())
    if username not in username_to_alias:
        if username == None:
            username_to_alias[username] = "[deleted]"
        else:
            username_to_alias[username] = "Commenter " + str(count+1)
        return username_to_alias[username]
    else:
        return username_to_alias[username]

# for name in usernames:
#     print(name)
#     print(deidentify(name))
#
# print(username_to_alias)

# will need something that also removes /u/username links, maybe?
