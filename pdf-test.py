# import pdfkit # pip install pdfkit
# also install wkhtmltopdf: https://wkhtmltopdf.org/downloads.html

#from weasyprint import HTML, CSS
import weasyprint

html_as_string = '''<div class="md"><p><strong>Edit: Wow, didn&#39;t expect so many replies. I appreciate all the feedback and respect your opinions, but I would like to note I&#39;m not asking for toxicity to run rampant. I understand in a long, competitive game like this its extremely annoying. I also would like to say that Yoga is in fact good for you, although doing it to prepare for a competitive game still seems a little too much.</strong> </p>

<p>Seriously, whats wrong with everyone? Does everyone think that the way you get better is fixing your mentality and meditating? We have posts about yoga, psychological excercises, anger issues, etc...</p>

<p>The true way to fix this is by just getting better at the game, which is what this subreddit is for.</p>

<p>Suck with winston? <a href="https://www.youtube.com/watch?v=Aqr1QmjVnEU&amp;t=156s">Heres a video by xqc about melee cancelling</a></p>

<p>Can&#39;t hit your shots? <a href="https://www.youtube.com/watch?v=JDbuuCs9ozY">Heres a video by surefour on how to improve your aim with movement</a> AND <a href="https://www.youtube.com/watch?v=1YS3dPWf6Xw">when to think about aiming.</a></p>

<p>Need help with Reinhardt? <a href="https://www.youtube.com/watch?v=UTb8CSa9tpc">Gamesense Rein guide by YourOverwatch, made 2 weeks ago.</a></p>

<p>Don&#39;t understand Ana? <a href="https://www.youtube.com/watch?v=glTW8MyFki0">ML7 released a multi-episode guide on how to use her.</a></p>

<p>Losing your 1v1s? <a href="https://workshop.codes/TXCXX">Heres an excellent code to 1v1 a friend, or a lobby of 8 people. Even if you don&#39;t download it, you can search it up on the game browser.</a></p>

<p>Getting wrecked by a smurf genji as mercy? <a href="https://www.youtube.com/watch?v=y6MQPGkXhHI">Heres a guide on mercy&#39;s super jump and when to apply it. Something EVERY mercy needs to know (Note: The music gets a little annoying, it sounds like something from Sonic Adventure 1)</a></p>

<p>We have so much material and so many people who are willing to share excellent information, that its a little disheartening watching people focus solely on something like, &quot;Some guy said I sucked with Baptiste on a quickplay match, so now I lost all hope in competitive as well as life.&quot; The solution isn&#39;t intense yoga, its ignoring the idiot, laughing at the fact that he took the time of day to message you on a quickplay game where you&#39;re trying to learn a character, its simply to reply, &quot;TrAsH&quot; and move on.</p>

<p>Did nobody ever play Cod? Halo? <strong>Mario Kart with friends?</strong> Toxicity is a gaming staple, and the best solution is to laugh at it, and get better.</p>
</div>'''

options = {
    'page-size': 'Letter',
    'margin-top': '1in',
    'margin-right': '1in',
    'margin-bottom': '1in',
    'margin-left': '1in',
    'encoding': "UTF-8",
    'custom-header' : [
        ('Accept-Encoding', 'gzip')
    ],
    'no-outline': None
}

#css = "pdf-style.css"

#pdfkit.from_string(html_as_string, 'test.pdf', options=options, css=css)

html = weasyprint.HTML(filename=html_filname)
css = CSS(filename="scraper-style.css")
weasyprint.HTML(string=html).write_pdf(stylesheets=[css])
