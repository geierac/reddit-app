import pdfkit
# pip install pdfkit
# also install wkhtmltopdf: https://wkhtmltopdf.org/downloads.html

def convert_to_pdf(filename):
    options = {
        'user-style-sheet': 'scraper-style.css',
        'page-size': 'Letter',
        'margin-bottom': '0.75in',
        'margin-left': '0.75in',
        'margin-right': '0.75in',
        'margin-top': '0.75in',
        #'enable-local-file-access': ""
    }
    pdfkit.from_file(filename, filename+".pdf", options=options)

# Currently doesn't recognize css file or any downloaded images.
