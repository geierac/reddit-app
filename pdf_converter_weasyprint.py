from weasyprint import HTML, CSS # pip install weasyprint first

def convert_to_pdf(html_filename):
    html = HTML(filename=html_filname)
    css = CSS(filename="scraper-style.css")
    html.write_pdf(html, stylesheets=[css])
