import requests
import shutil

# reference: https://towardsdatascience.com/how-to-download-an-image-using-python-38a75cfa21c

def is_url_image(image_url):
   image_formats = ("image/png", "image/jpeg", "image/jpg")
   r = requests.head(image_url)
   if r.headers["content-type"] in image_formats:
      return True
   return False

def image_grab(url):
    image_url = url
    filename = image_url.split("/")[-1]
    # something here to check if url is actually an image and not just a link
    # extensions to check for: png, jpg, jpeg, gif, bmp, tiff, svg

    if is_url_image(url) == False:
        #print("Not an image")
        return False

    r = requests.get(image_url, stream = True)

    if r.status_code == 200:
        r.raw.decode_content = True

        with open("images/"+filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

        print("Image download successful")
        return filename

    else:
        print("Error: couldn't get image")
        return False

image_grab("https://www.reddit.com/r/CoronavirusMichigan/comments/mm799d/cdc_director_wants_stronger_restrictions_in/")
