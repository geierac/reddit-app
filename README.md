# Reddit Scraper

For scraping Reddit for discourse analysis project, 2021

Instructions for use:
Install dependencies first (see below). Add Reddit credentials and API key to redditconfig.py. To run program in PowerShell/Terminal, use this command:
python scraper.py [paste url of Reddit post here]
Note MacOS users should use python3.
Program will generate an HTML file in the program's root folder named based on the post ID.

Features:
[x] Takes as input the URL for a specific post
[x] Captures original post content and author
[x] Captures ALL comments and represents them hierarchically
[x] Upvotes and upvote ratio for post
[x] Score for comments
[x] All text formatting features (bold, italic, strikethrough, etc.) preserved
[x] Emojis preserved
[x] Images embedded rather than linked
[x] All post and comment author usernames replaced with aliases
[x] Marker for "is original poster" intact

Author: Caitlin Geier

Install these before using:
-----------------------
pip install weasyprint

TODOs
----------

Future (maybe)
---------------
[ ] Allow for changing order comments are displayed in (note: default is "best")
[ ] Marker for "is mod" intact
[ ] All output saved as formatted PDF file
[ ] Show whether a post was deleted or removed by mods. Possibly helpful: https://www.reddit.com/r/redditdev/comments/8pekxv/check_if_comment_is_removed_by_mod_as_a_mod/

Changelog
--------------
4/16/21
- Added code to deidentifier to show username for deleted posts as "[deleted]" rather than all as the same commenter.

4/13/21
- modified image_grab.py so it will test to see if the url is an image before trying to pull it. Also modified scraper.py so if the submission.url is a link, it shows the link in place of the image or textual post content.
- hacked away at getting pdf conversion to work. WeasyPrint has lots of dependencies I couldn't get to work. pdfkit / wkhtmltopdf have issue with version 0.12.6 and later where it won't allow access to local files (e.g. my css and image files). Managed to embed the css, but @media seems to not work. Note that css in @media print section DOES work when using browser to print to PDF.
- made username_deidentifier module which adds usernames to a dictionary matched with a unique alias and then returns the alias. Changed com_author definition at top of print_comments() function in scraper.py to use deidentifier. Tested output with multiple posts, I think it's good
- removed the post author from the post metadata. In comments, replaced post author username with "Original Poster"
- made a more unique title in the html head using the post id and the current date (date scraped). Also included date scraped in the post metadata.
- looked into checking if a commenter is a moderator for the subreddit, but couldn't find anything. is_mod for a redditor only checks if they're a mod for any subreddit, not that particular one. Will have to do more research
- added decision tree to determine if a comment score is positive or negative, then <span> classes to represent positive and negative scores in different colors
- might need to figure out a way to move .html files into a different folder so i can .gitignore it. But then where to put the css stylesheet?

4/8/21
- added an if/else tree for comment author so that OP (original poster) has an OP callout next to their username in the comments. Note: after anonymizing, could alter this so OP is simply called "original poster" everywhere.
- created image_grab module which downloads image from link; changed code in scraper.py so that if image exists, it's embedded in the html. NOTE: this does not handle if the url link given for the post is a link to a webpage and not an image!!
- tried to incorporate PDF converter code, but couldn't get it to recognize css or images in creating the PDF. Could try something other than pdfkit: weasyprint looks promising. https://weasyprint.readthedocs.io/en/stable/tutorial.html
- may want to change the html title so it's dynamic - post name or something

4/6/21
- got the entire comment tree to print in the correct hierarchy! (don't use .list() - lesson learned)
- incorporated html framework (head, body, etc.) and stylesheet into html file
- changed file encoding attribute to support emoji (utf8)
- changed the url in the post metadata so it displays the permalink all the time
- tested downloading an image based on a url - see image-test.py. Next step: incoporate into scraper.py. Use submission.url to get the url, send to images folder. Then include some code in else statement in "write post metadata to file" section to embed the image into the html by reference to its location in the images folder. Hopefully this will also work when I eventually include the html to pdf part.

3/25/21
- Got wkhtmltopdf to work; needed to add environmental variable path. See https://pythonexamples.org/python-convert-html-to-pdf/. pdf-test.py works, but couldn't get css file to load properly. Maybe add a css file reference to the .html file I generate in scraper.py instead?
- Converted all post metadata + html text into strings and wrote into an .html file (to be converted to PDF later)
- Have the first 2 levels of comments printed into the .html file, but nothing beyond that. Look a bit more into CommmentForest and the replies attribute for the comment object.
- Will need css for comment tree level, blockquote, font styling and spacing

3/23/21
- Prints out metadata and text from post via URL supplied when running program.
Also has loop for printing out all of the comments associated with the post, with HTML intact. Don't yet have metadata for comments, or a way to represent hierarchical structure. Should maybe work on this next...?
Started working on converting html to PDF with pdfkit (see pdf-test.py). Couldn't get it to work because install of wkhtmltopdf wasn't recognized by python / command line. Maybe try another library (e.g. from https://gearheart.io/blog/how-generate-pdf-files-python-xhtml2pdf-weasyprint-or-unoconv/) or think of other solutions.
