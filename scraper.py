import praw
import sys
import pprint
from datetime import datetime, date
from redditconfig import * # reddit api configuration information
from image_grab import *  # for downloading the image associated with a post, if there is one
from pdf_converter import * # converts the html this program outputs to a pdf
from username_deidentifier import * # converts comment author names to aliases

# pulls credentials from reddiconfig.py
reddit = praw.Reddit(
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET,
    user_agent=USER_AGENT,
    username=USERNAME,
    password=PASSWORD)

# converts the UTC timestamp to something human readable
def convert_create_date(created):
    date = datetime.utcfromtimestamp(created)
    return date.strftime("%B %d, %Y at %H:%M UTC")

# pulls Reddit post by URL. In command line, use command: py scraper.py <url>
input_url = sys.argv[1]
submission = reddit.submission(url=input_url)

# setting up html document framework
filename = "post-" + submission.id + ".html"
today = date.today()
current_date = today.strftime("%B %d, %Y")

html_file = open(filename, "w")
html_start = '''<!DOCTYPE html>
<html lang="en-US">
  <head>
    <title>Post ID: ''' + submission.id + ''' scraped on ''' + current_date + '''</title>
    <link rel="stylesheet" href="scraper-style.css">
  </head>
  <body>
'''
html_file.write(html_start)
html_file.close()

# gets metadata and html body of the original post
# should these all be html-ified first?
title = "<h1>" + str(submission.title) + "</h1>"
post_author = "<p>Post author: " + str(submission.author) + "</p>"
url = '<p>URL: <a href="https://www.reddit.com' + str(submission.permalink) + '">' + str(submission.permalink) + "</a></p>"
score = "<p>Score: " + str(submission.score) + "</p>"
upvote_ratio = "<p>Upvote ratio: " + str(submission.upvote_ratio) + "</p>"
num_comments = "<p>Number of comments: " + str(submission.num_comments) + "</p>"
post_date_convert = convert_create_date(submission.created_utc)
post_date = "<p>Created: " + str(post_date_convert) + "</p>"
scraped_date = "<p>Scraped: " + current_date + "</p><hr>"

# download and embed an image if there is one


post_data = [title, url, score, upvote_ratio, num_comments, post_date, scraped_date] # post metadata in a list to iterate through later
# removed post author

# writes post metadata and body to html file
html_file = open(filename, "a", encoding="utf8")
for item in post_data:
    html_file.writelines(item)
# if text-only, just print text. Otherwise embed image
if submission.is_self:
    post_text = str(submission.selftext_html) + "<hr><hr>"
    html_file.writelines(post_text)
elif image_grab(submission.url) == False:
    html_file.writelines('<div class="linked-url">Link: <a href="'+ str(submission.url) + '">' + str(submission.url) + '</a></div><hr><hr>')
else:
    html_file.writelines('<img src="images/' + image_grab(submission.url) + '" width = "600"><hr><hr>')
    # only handles if link is to an image. May need to handle external links as well
html_file.close()

# Need something here that sorts comments based on the reddit "best" algorithm - or maybe a choice of algorithmic sorting using sys.arg? Default seems to be "best" right now

# gets HTML output and other metadata for each comment

html_file = open(filename, "a", encoding="utf8")
submission.comments.replace_more(limit=None)
comment_queue = submission.comments

def print_comments(comment, level=1): # function for creating the comment tree
    # com_author = str(comment.author)

    # uses deidentifier module to change username to alias
    com_author = deidentify(comment.author)

    # changes color of comment score depending on whether it's more or less than 0
    if comment.score < 0:
        com_score = '<p class="comment-score">Score: <span class="negative-score">' + str(comment.score) + '</span></p>'
    else:
        com_score = '<p class="comment-score">Score: <span class="positive-score">' + str(comment.score) + '</span></p>'
    com_content = str(comment.body_html)

    if level > 1:
        padding = str((level-1)*15)
        html_file.writelines('<div class="comment comment-level-'+str(level%5)+'" style="margin-left:'+padding+'px">')
    else:
        html_file.writelines('<div class="comment comment-level-base">')

    # add marker next to username is comment author is also original poster (OP)
    if comment.is_submitter == True:
        #html_file.writelines('<p class="comment-author">' + com_author + ' | <span class="is-op">OP</span></p>') # replace with just "OP" or "Original Poster" to deidentify
        html_file.writelines('<p class="comment-author"><span class="is-op">Original Poster</span></p>')
    else:
        html_file.writelines('<p class="comment-author">' + com_author + '</p>')

    html_file.writelines(com_content)
    html_file.writelines(com_score)
    html_file.writelines('</div>')

    for reply in comment.replies:
        print_comments(reply, level+1)

for comment in comment_queue: # iterates through comments and prints the comment tree
    print_comments(comment)

html_file.close()

# finishing off the html framwork
html_file = open(filename, "a")
html_file.write('''</body>
</html>
''')
html_file.close()

# converts html file + attributes to pdf
# convert_to_pdf(filename)



# Stuff I want from the post:
# selftext_html gets the post in html
# subreddit or subreddit_name_prefixed - name of subreddit
# title - submission.title will do that
# author
# created_utc - create date and time, will need to use something to convert this to human readable time
# url - there's a url field, can also use input_url variable above
# ups - Upvotes
# upvote_ratio - upvote ratio, obviously
# score - I think this is the upvote/downvote number that shows next to the post
# num_comments
# _comments_by_id gives a list of the id numbers of all the comments associated with the post, but no parent ids
